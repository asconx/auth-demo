#!/usr/bin/env bash
# Speichert den public key in der Datei, die Postgrest verwendet.
curl http://localhost:9999/auth/realms/test-realm/protocol/openid-connect/certs -o docker/postgrest/keycloak.public.key
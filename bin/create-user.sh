#!/usr/bin/env bash
docker run --name=kcadm.sh --network="host" --rm  --entrypoint /bin/bash jboss/keycloak -c "alias kcadm.sh=/opt/jboss/keycloak/bin/kcadm.sh && \
     /opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:9999/auth --realm master --user admin --password admin && \
     /opt/jboss/keycloak/bin/kcadm.sh create users --target-realm test-realm --set username=user1 --set email=user1@testdomain.nl --set attributes.APP_ROLE=test_app_admin --set enabled=true --output --fields id,username && \
     /opt/jboss/keycloak/bin/kcadm.sh set-password --target-realm test-realm --username user1 --new-password test && \
     /opt/jboss/keycloak/bin/kcadm.sh add-roles --target-realm test-realm --uusername user1 --rolename TEST_APP_ADMIN
"

#!/usr/bin/env bash
docker-compose -f docker/docker-compose.yml -p demo_app stop $1
docker-compose -f docker/docker-compose.yml -p demo_app up -d --no-deps $1
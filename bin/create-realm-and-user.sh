#!/usr/bin/env bash
echo $PWD
docker run -v "${PWD}/docker/keycloak:/var/keycloak"  --name=kcadm.sh --network="host" --rm  --entrypoint /bin/bash jboss/keycloak -c "alias kcadm.sh=/opt/jboss/keycloak/bin/kcadm.sh && \
     /opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:9999/auth --realm master --user admin --password admin && \
     /opt/jboss/keycloak/bin/kcadm.sh create realms --set realm=test-realm --set enabled=true && \
     /opt/jboss/keycloak/bin/kcadm.sh update realms/test-realm -f /var/keycloak/realm.json && \
     /opt/jboss/keycloak/bin/kcadm.sh create clients --target-realm test-realm --set clientId=login-app --set directAccessGrantsEnabled=true --set publicClient=true --set 'redirectUris=[\"http://localhost:8085/*\"]' --id && \
     /opt/jboss/keycloak/bin/kcadm.sh create roles --target-realm test-realm --set name=TEST_APP_ADMIN -o && \
     /opt/jboss/keycloak/bin/kcadm.sh create users --target-realm test-realm --set username=user1 --set email=user1@testdomain.nl --set enabled=true --output --fields id,username && \
     /opt/jboss/keycloak/bin/kcadm.sh set-password --target-realm test-realm --username user1 --new-password test && \
     /opt/jboss/keycloak/bin/kcadm.sh add-roles --target-realm test-realm --uusername user1 --rolename TEST_APP_ADMIN
"

#!/usr/bin/env bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    -- Create API
    create schema api;

    -- The password needs to be changed with root rights after this script.
    create role authenticator noinherit login password 'mysecret-initial-password';

    create role TEST_APP_ADMIN nologin;
    grant TEST_APP_ADMIN to authenticator;

    create role web_anon;
    grant web_anon to authenticator;


    grant usage on schema api to TEST_APP_ADMIN;

    create table api.test();

    grant select on api.test to TEST_APP_ADMIN;


    ALTER TABLE api.test ENABLE ROW LEVEL SECURITY;

    CREATE POLICY user_policy ON api.test
        USING (email = current_setting('request.jwt.claim.email', true));
EOSQL